<html>
<body>
<link rel="stylesheet" type="text/css" href="css/style.css">
<h1>一言掲示板</h1>
<script type="text/javascript" src="js/validate.js"></script>
<p><?= $_SESSION['member']['name'] ?>でログイン中 <a href="logout.php">ログアウト</a></p>
<table border=1>
    <tr id="orange"><th>名前</th><th>コメント</th><th>時刻</th></tr>
    <?php foreach($data as $key => $value){
            $updated[$key] = $value["date"];
        }
       array_multisort($updated, SORT_DESC, $data); 
        ?>
    <?php foreach($data as $row): ?>
<form action="delete.php" method="POST">
    <tr>
    <td><?= html_escape($row['name']);?></td>
    <td><?= nl2br(html_escape($row['msg']));?></td>
    <td><?= html_escape($row['date']);?></td>
    <input type="hidden" name="seqno" value="<?=$row['seqno']?>">
    <td><?php if($_SESSION['member']['id'] == $row['id']) echo "<input type='submit' value='削除' >";?></td>
    </tr>
</form>
    <?php endforeach; ?>
</table>
<?php if(count($errs)){
    foreach($errs as $err){
        echo '<p style="color: red">'.$err.'</p>';
    }
}?>
<form action="" method="POST" onsubmit="checkForm()" name="message">
<p>お名前*<input type="text" name="name"></p>
<p>ひとこと*<textarea name="msg" rows="4" cols="40"></textarea>(140文字まで)</p>
<input type="submit" value="書き込む">
</form>
</body>
</html>
