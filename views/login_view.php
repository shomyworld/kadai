<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
</head>
<body>
<h1>ログイン</h1>
<form action="login.php" method="POST">
<p>ID：<input type="text" name="id"> <?php echo html_escape($errs['id']); ?></p>
<p>パスワード：<input type="password" name="password"> <font color="red"><?php echo html_escape($errs['password']); ?></font></p>
<p><input type="submit" value="ログイン"></p>
</form>
</body>
</html>
