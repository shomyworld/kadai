<?php

require_once('config.php');
require_once('./helpers/db_helper.php');
require_once('./helpers/extra_helper.php');

session_start();
//すでにログイン済みだったらboard.phpへリダイレクト
if (!empty($_SESSION['member'])) {
    header('Location: '.SITE_URL.'/board.php');
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $id = get_post('id');
    $password = get_post('password');

    $dbh = get_db_connect();
    $errs = array();

    //IDとパスワードが一致するか検証する
    if (!check_words($password, 50)) {
        $errs['password'] = 'パスワードは必須、50文字以下で入力してください';
    } elseif (!$member = select_member($dbh, $id, $password)) {
        $errs['password'] = 'パスワードとIDが正しくありません';
    }
    //ログインする
    if (empty($errs)) {
        session_regenerate_id(true);
        $_SESSION['member'] = $member;
        update_lastlogin($dbh,$id);
        header('Location: '.SITE_URL.'board.php');
        exit;
    }

}

include_once('./views/login_view.php');
