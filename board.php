<?php
require_once('config.php');
require_once('./helpers/db_helper.php');
require_once('./helpers/extra_helper.php');

session_start();

// ログインしてなかったらログイン画面に戻す
if (empty($_SESSION['member'])) {
    header('Location: '.SITE_URL.'login.php');
    exit;
}

//データベースへの接続
$dbh = get_db_connect();
$errs = [];
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    //POSTデータの取得
    $name = get_post('name');
    $msg = get_post('msg');
    //文字数のチェック
    if (!check_words($name, 50)) {
        $errs[] = 'お名前欄を修正してください';
    }
    if (!check_words($msg, 200)) {
        $errs[] = 'コメント欄を修正してください';
    }

    if(count($errs) === 0){
    //コメントの書き込み
    $result = insert_comment($dbh, $_SESSION['member']['id'], $name, $msg);
    }
}

//全コメントデータの取得
$data = select_comments($dbh);

include_once('./views/board_view.php');
