<?php

// データーベースに接続する
function get_db_connect() {
try{
    $dsn = DSN;
    $user = DB_USER;
    $password = DB_PASSWORD;

    $dbh = new PDO($dsn, $user, $password);
    }catch (PDOException $e){
       echo($e->getMessage());
       die();
    }
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}


function select_member($dbh, $id, $password) {

    $sql = 'SELECT * FROM user_1280336 WHERE id = :id LIMIT 1';
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    if($stmt->rowCount() > 0 ){
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        if($password==$data['password']){
            return $data;
        }else{
            return FALSE;
        }
    }else{
        return FALSE;
    }
}

// メンバーを取得する
function select_members($dbh) {

    $sql = "SELECT name FROM user_1280336";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $data[] = $row;
    }
    return $data;
}

// lastloginカラムを現在日時で更新する
function update_lastlogin($dbh, $id){
    $sql = "UPDATE user_1280336 SET lastlogin = '".date("Y-m-d H:i:s")."' WHERE id = :id";
    var_dump($sql);
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
}


//コメントを書き込む
function insert_comment($dbh, $id, $name, $msg){

    $date = date('Y-m-d H:i:s');
    $sql = "INSERT INTO msg_1280336 (id, name, msg, date) VALUE (:id, :name, :msg, '{$date}')";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':id', $id, PDO::PARAM_STR);
    $stmt->bindValue(':name', $name, PDO::PARAM_STR);
    $stmt->bindValue(':msg', $msg, PDO::PARAM_STR);
    if(!$stmt->execute()){
        return 'データの書き込みに失敗しました。';
    }
}


//全コメントデータを取得する
function select_comments($dbh) {

    $sql = "SELECT seqno, id, name, msg, date FROM msg_1280336";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $data[] = $row;
    }
    return $data;
}

// コメント削除
function delete_msg($dbh, $seqno){
        $sql = "DELETE FROM msg_1280336 WHERE seqno = :seqno";
        $stmt = $dbh->prepare($sql);
        $stmt->bindValue(':seqno', $seqno, PDO::PARAM_STR);
        $stmt->execute();
    }
